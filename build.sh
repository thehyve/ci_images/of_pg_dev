#!/usr/bin/env bash
NEWCONTAINER=$(buildah from postgres:${PG_VER})
buildah run ${NEWCONTAINER} apt update
buildah run ${NEWCONTAINER} apt install -y gcc make postgresql-server-dev-${PG_VER}
buildah run ${NEWCONTAINER} apt-get clean
buildah commit ${NEWCONTAINER} ${CI_PROJECT_NAME}
buildah tag ${CI_PROJECT_NAME} ${CI_REGISTRY_IMAGE}:${PG_VER}
buildah push --creds ${CI_REGISTRY_USER}:${CI_JOB_TOKEN} ${CI_REGISTRY_IMAGE}:${PG_VER}
